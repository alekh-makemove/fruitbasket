<?php

require __DIR__ . '/params.php';

// Cache
$app['cache.path'] = __DIR__ . '/../cache';
// Http cache
$app['http_cache.cache_dir'] = $app['cache.path'] . '/http';

// Doctrine (db)
$app['db.options'] = [
    'driver' => $app['app.params']['db']['driver'],
    'charset' => 'utf8',
    'host' => $app['app.params']['db']['host'],
    'dbname' => $app['app.params']['db']['dbname'],
    'user' => $app['app.params']['db']['user'],
    'password' => $app['app.params']['db']['password']
];
