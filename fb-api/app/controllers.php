<?php

use Symfony\Component\HttpFoundation\Response;
use FruitBasket\Provider\Controller\BasketControllerProvider;
use FruitBasket\Provider\Controller\ItemControllerProvider;

$app->mount('/basket', new BasketControllerProvider());
$app->mount('/item', new ItemControllerProvider());

$app->error(function(\Exception $e, $code) use($app) {
    if ($app['debug']) {
        return;
    }
    switch ($code) {
        case 404:
            $message = 'The requested page could not be found.';
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }
    
    return new Response($message, $code);
});

return $app;
