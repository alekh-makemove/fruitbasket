<?php

use FruitBasket\Service\BasketService;
use FruitBasket\Service\ItemService;

$app['basket_service'] = function () use($app) {
    return new BasketService($app);
};

$app['item_service'] = function () use($app) {
    return new ItemService($app);
};