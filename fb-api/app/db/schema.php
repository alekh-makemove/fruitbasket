<?php

use Doctrine\DBAL\Schema\Schema;

$schema = new Schema();

$basket = $schema->createTable('baskets');
$basket->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
$basket->addColumn('name', 'string', ['length' => 32]);
$basket->addColumn('capacity', 'integer');
$basket->addColumn('views', 'integer');
$basket->setPrimaryKey(['id']);
$basket->addUniqueIndex(['name']);

$item = $schema->createTable('items');
$item->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
$item->addColumn('name', 'string', ['length' => 32]);
$item->addColumn('weight', 'integer');
$item->setPrimaryKey(['id']);
$item->addUniqueIndex(['name']);

$basketItem = $schema->createTable('baskets_items');
$basketItem->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
$basketItem->addColumn("basket_id", "integer", ['unsigned' => true]);
$basketItem->addColumn("item_id", "integer", ['unsigned' => true]);
$basketItem->addColumn("quantity", "integer");
$basketItem->setPrimaryKey(['id']);
$basketItem->addForeignKeyConstraint($basket, ['basket_id'], ['id'], ["onDelete" => "CASCADE"]);
$basketItem->addForeignKeyConstraint($item, ['item_id'], ['id'], ["onDelete" => "CASCADE"]);

return $schema;
