<?php
namespace FruitBasket;

class StringUtils {

    public static function isBlankOrNull($string) {
        return $string === null || trim($string) === '';
    }

}
