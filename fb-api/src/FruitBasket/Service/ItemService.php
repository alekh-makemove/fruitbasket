<?php
namespace FruitBasket\Service;

use FruitBasket\StringUtils;
use FruitBasket\FruitBasketException;

class ItemService {

    public $app;

    public function __construct($app) {
        $this->app = $app;
    }

    public function createNewItem($name, $weight) {
        if (StringUtils::isBlankOrNull($name)) {
            throw new FruitBasketException('Name is required.');
        } elseif ($weight <= 0) {
            throw new FruitBasketException('Weight should be bigger then 0.');
        } elseif ($this->checkItemExists(null, $name)) {
            throw new FruitBasketException('Item with this name already exists.');
        }

        $this->app['db']->insert('items', ['name' => $name, 'weight' => $weight]);
    }

    public function renameItemById($name, $id) {
        if (StringUtils::isBlankOrNull($name)) {
            throw new FruitBasketException('Name is required.');
        } elseif (!$this->checkItemExists($id)) {
            throw new FruitBasketException('Item doesn\'t exists.');
        } elseif ($this->checkItemExists(null, $name)) {
            throw new FruitBasketException('Item with this name already exists.');
        }

        $this->app['db']->update('items', ['name' => $name], ['id' => $id]);
    }

    public function removeItemById($id) {
        if (!$this->checkItemExists($id)) {
            throw new FruitBasketException('Item doesn\'t exists.');
        }

        $this->app['db']->delete('items', ['id' => $id]);
    }

    public function getItemById($id) {
        $qb = $this->app['db']->createQueryBuilder();

        $item = $qb
                    ->select('*')
                    ->from('items')
                    ->where('id = :id')
                    ->setParameter('id', $id)
                    ->execute()->fetch();

        if (empty($item)) {
            throw new FruitBasketException('Item doesn\'t exists.');
        }

        return $item;
    }

    public function getItemsList() {
        return $this->app['db']->fetchAll("SELECT * FROM items");
    }

    public function checkItemExists($id = null, $name = null) {
        if ($id === null && $name === null) {
            $return = false;
        } else {
            $qb = $this->app['db']->createQueryBuilder();
            $qb->select('COUNT(id)')->from('items');

            if ($id !== null) {
                $qb->orWhere('id = :id')->setParameter('id', $id);
            }
            if ($name !== null) {
                $qb->orWhere('name = :name')->setParameter('name', $name);
            }

            $return = $qb->execute()->fetchColumn() > 0;
        }

        return $return;
    }

}
