<?php
namespace FruitBasket\Service;

use FruitBasket\StringUtils;
use FruitBasket\FruitBasketException;

class BasketService {

    public $app;

    public function __construct($app) {
        $this->app = $app;
    }

    public function createNewBasket($name, $capacity) {
        if (StringUtils::isBlankOrNull($name)) {
            throw new FruitBasketException('Name is required.');
        } elseif ($capacity <= 0) {
            throw new FruitBasketException('Capacity should be bigger then 0.');
        } elseif ($this->checkBasketExists(null, $name)) {
            throw new FruitBasketException('Basket with this name already exists.');
        }

        $this->app['db']->insert('baskets', ['name' => $name, 'capacity' => $capacity]);
    }

    public function getBasketById($id) {
        $qb = $this->app['db']->createQueryBuilder();

        $basket = $qb
                    ->select('*')
                    ->from('baskets')
                    ->where('id = :id')
                    ->setParameter('id', $id)
                    ->execute()->fetch();

        if (empty($basket)) {
            throw new FruitBasketException('Basket doesn\'t exists.');
        }

        return $basket;
    }
    
    public function increaseBasketViews($basket) {
        $this->app['db']->update('baskets', ['views' => $basket['views'] + 1], ['id' => $basket['id']]);
    }

    public function renameBasketById($name, $id) {
        if (StringUtils::isBlankOrNull($name)) {
            throw new FruitBasketException('Name is required.');
        } elseif (!$this->checkBasketExists($id)) {
            throw new FruitBasketException('Basket doesn\'t exists.');
        } elseif ($this->checkBasketExists(null, $name)) {
            throw new FruitBasketException('Basket with this name already exists.');
        }

        $this->app['db']->update('baskets', ['name' => $name], ['id' => $id]);
    }

    public function removeBasketById($id) {
        if (!$this->checkBasketExists($id)) {
            throw new FruitBasketException('Basket doesn\'t exists.');
        }

        $this->app['db']->delete('baskets', ['id' => $id]);
    }

    public function getBasketsList() {
        return $this->app['db']->fetchAll("SELECT * FROM baskets");
    }

    public function checkBasketExists($id = null, $name = null) {
        if ($id === null && $name === null) {
            $return = false;
        } else {
            $qb = $this->app['db']->createQueryBuilder();
            $qb->select('COUNT(id)')->from('baskets');

            if ($id !== null) {
                $qb->orWhere('id = :id')->setParameter('id', $id);
            }
            if ($name !== null) {
                $qb->orWhere('name = :name')->setParameter('name', $name);
            }

            $return = $qb->execute()->fetchColumn() > 0;
        }

        return $return;
    }

    public function getBasketItems($basketId, $itemId = null) {
        $qb = $this->app['db']->createQueryBuilder();
        $qb
            ->select('bi.*, i.weight')
            ->from('baskets_items', 'bi')
            ->join('bi', 'items', 'i', 'i.id = bi.item_id')
            ->where('basket_id = :basketId')
            ->setParameter('basketId', $basketId);

        if ($itemId !== null) {
            $qb->andWhere('item_id = :itemId')->setParameter('itemId', $itemId);
            $return = $qb->execute()->fetch();
        } else {
            $return = $qb->execute()->fetchAll();
        }

        return $return;
    }

    public function addItemsToBasket($basketId, $items) {
        if(empty($items)) {
            throw new FruitBasketException('Please add any items.');
        }

        $needWeight = $usedWeight = 0;
        foreach ($this->getBasketItems($basketId) as $item) {
            $usedWeight += $item['quantity'] * $item['weight'];
            $basketItems[$item['item_id']] = [
                'bi_id' => $item['id'],
                'quantity' => $item['quantity'],
                'weight' => $item['weight']
            ];
        }
        
        foreach($items as $item) {
            if(!isset($item['id']) || !isset($item['quantity'])) {
                throw new FruitBasketException('Bad request. Please check json parameters.');
            }
            
            if(isset($basketItems[$item['id']])) {
                $itemData['weight'] = $basketItems[$item['id']]['weight'];
            } else {
                $itemData = $this->app['item_service']->getItemById($item['id']);
            }
            
            $needWeight += $itemData['weight'] * $item['quantity'];
        }
        
        $basket = $this->getBasketById($basketId);
        $freeWeight = $basket['capacity'] - $usedWeight;
        if ($freeWeight < $needWeight) {
            throw new FruitBasketException('Basket is overflowing. Weight needed: ' . $needWeight . '. Free weight: ' . $freeWeight);
        } else {
            foreach($items as $item) {
                if(isset($basketItems[$item['id']])) {
                    $this->app['db']->update(
                                        'baskets_items', 
                                        ['quantity' => $basketItems[$item['id']]['quantity'] + $item['quantity']],
                                        ['id' => $basketItems[$item['id']]['bi_id']]
                                    );
                } else {
                    $this->app['db']->insert(
                                        'baskets_items', 
                                        [
                                            'quantity' => $item['quantity'],
                                            'item_id' => $item['id'],
                                            'basket_id' => $basketId
                                        ]
                                    );
                }
            }
        }
    }

    public function removeItemFromBasket($basketId, $itemId) {
        $basketItems = $this->getBasketItems($basketId, $itemId);
        if(!empty($basketItems)) {
            if($basketItems['quantity'] == 1) {
                $this->app['db']->delete('baskets_items', ['id' => $basketItems['id']]);
            } else {
                $this->app['db']->update('baskets_items', ['quantity' => $basketItems['quantity'] - 1], ['id' => $basketItems['id']]);
            }
        }
    }
    
    public function basketResponseData($basket) {
        $basketItems = $this->getBasketItems($basket['id']);
        foreach($basketItems as $item) {
            $basket['items'][] = [
                'item_id' => $item['item_id'],
                'quantity' => $item['quantity']
            ];
        }
        
        return $basket;
    }

}
