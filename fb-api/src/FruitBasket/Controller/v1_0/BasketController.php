<?php
namespace FruitBasket\Controller\v1_0;

use Silex\Application;
use FruitBasket\Controller\BaseController;
use FruitBasket\FruitBasketException;
use Symfony\Component\HttpFoundation\Request;

class BasketController extends BaseController {

    private $app;

    public function __construct(Application $app) {
        $this->app = $app;
    }

    /**
     * Create new basket.
     * 
     * @param Request $request
     * @return JsonResponse
     */
    public function addBasketAction(Request $request) {
        if ($requestData = $this->getParameterBag($request)) {
            $name = $requestData->get('name');
            $capacity = $requestData->get('capacity');

            try {
                $this->app['basket_service']->createNewBasket($name, $capacity);
                $response = $this->successJsonResponse();
            } catch (FruitBasketException $e) {
                $response = $this->errorJsonResponse($e->getMessage());
            } catch (Exception $e) {
                $this->app['monolog']->critical($e);
                $response = $this->errorJsonResponse($e->getMessage());
            }
        } else {
            $response = $this->errorJsonResponse();
        }

        return $response;
    }

    /**
     * Get all baskets.
     * 
     * @return JsonResponse
     */
    public function basketsListAction() {
        try {
            $baskets = $this->app['basket_service']->getBasketsList();
            $response = $this->successJsonResponse($baskets);
        } catch (FruitBasketException $e) {
            $response = $this->errorJsonResponse($e->getMessage());
        } catch (Exception $e) {
            $this->app['monolog']->critical($e);
            $response = $this->errorJsonResponse($e->getMessage());
        }

        return $response;
    }

    /**
     * Get basket by id.
     * 
     * @return JsonResponse
     */
    public function getBasketAction($id) {
        try {
            $bs = $this->app['basket_service'];
            
            $basket = $bs->getBasketById($id);
            $bs->increaseBasketViews($basket);
            $response = $this->successJsonResponse($bs->basketResponseData($basket));
        } catch (FruitBasketException $e) {
            $response = $this->errorJsonResponse($e->getMessage());
        } catch (Exception $e) {
            $this->app['monolog']->critical($e);
            $response = $this->errorJsonResponse($e->getMessage());
        }

        return $response;
    }

    /**
     * Rename basket by id.
     * 
     * @param Request $request
     * @param integer $id
     * @return JsonResponse
     */
    public function renameBasketAction(Request $request, $id) {
        if ($requestData = $this->getParameterBag($request)) {
            $name = $requestData->get('name');

            try {
                $this->app['basket_service']->renameBasketById($name, $id);
                $response = $this->successJsonResponse();
            } catch (FruitBasketException $e) {
                $response = $this->errorJsonResponse($e->getMessage());
            } catch (Exception $e) {
                $this->app['monolog']->critical($e);
                $response = $this->errorJsonResponse($e->getMessage());
            }
        } else {
            $response = $this->errorJsonResponse();
        }

        return $response;
    }

    /**
     * Remove basket by id.
     * 
     * @param type $id
     * @return JsonResponse
     */
    public function removeBasketAction($id) {
        try {
            $this->app['basket_service']->removeBasketById($id);
            $response = $this->successJsonResponse();
        } catch (FruitBasketException $e) {
            $response = $this->errorJsonResponse($e->getMessage());
        } catch (Exception $e) {
            $this->app['monolog']->critical($e);
            $response = $this->errorJsonResponse($e->getMessage());
        }

        return $response;
    }

    /**
     * Add items to basket
     * 
     * @param type $basketId
     * @return type
     */
    public function addItemsToBasketAction(Request $request, $basketId) {
        if ($requestData = $this->getParameterBag($request)) {
            $items = $requestData->get('items', []);
            
            try {
                $this->app['basket_service']->addItemsToBasket($basketId, $items);
                $response = $this->successJsonResponse();
            } catch (FruitBasketException $e) {
                $response = $this->errorJsonResponse($e->getMessage());
            } catch (Exception $e) {
                $this->app['monolog']->critical($e);
                $response = $this->errorJsonResponse($e->getMessage());
            }
        } else {
            $response = $this->errorJsonResponse();
        }
        
        return $response;
    }

    /**
     * Remove item from basket
     * 
     * @param type $basketId
     * @param type $itemId
     * @return type
     */
    public function removeItemFromBasketAction($basketId, $itemId) {
        try {
            $this->app['basket_service']->removeItemFromBasket($basketId, $itemId);
            $response = $this->successJsonResponse();
        } catch (FruitBasketException $e) {
            $response = $this->errorJsonResponse($e->getMessage());
        } catch (Exception $e) {
            $this->app['monolog']->critical($e);
            $response = $this->errorJsonResponse($e->getMessage());
        }

        return $response;
    }

}
