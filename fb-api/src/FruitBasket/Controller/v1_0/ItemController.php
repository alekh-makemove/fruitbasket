<?php
namespace FruitBasket\Controller\v1_0;

use Silex\Application;
use FruitBasket\Controller\BaseController;
use FruitBasket\FruitBasketException;
use Symfony\Component\HttpFoundation\Request;

class ItemController extends BaseController {
    
    private $app;
    
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Create new item.
     * 
     * @param Request $request
     * @return JsonResponse
     */
    public function addItemAction(Request $request) {
        if($requestData = $this->getParameterBag($request)) {
            $name = $requestData->get('name');
            $weight = $requestData->get('weight');
            
            try {
                $this->app['item_service']->createNewItem($name, $weight);
                $response = $this->successJsonResponse();
            } catch(FruitBasketException $e) {
                $response = $this->errorJsonResponse($e->getMessage());
            } catch (Exception $e) {
                $this->app['monolog']->critical($e);
                $response = $this->errorJsonResponse($e->getMessage());
            }
        } else {
            $response = $this->errorJsonResponse();
        }
        
        return $response;
    }
    
    /**
     * Get all items.
     * 
     * @return JsonResponse
     */
    public function itemsListAction() {
        try {
            $items = $this->app['item_service']->getItemsList();
            $response = $this->successJsonResponse($items);
        } catch(FruitBasketException $e) {
            $response = $this->errorJsonResponse($e->getMessage());
        } catch (Exception $e) {
            $this->app['monolog']->critical($e);
            $response = $this->errorJsonResponse($e->getMessage());
        }
        
        return $response;
    }
    
    /**
     * Get item by id.
     * 
     * @return JsonResponse
     */
    public function getItemAction($id) {
        try {
            $item = $this->app['item_service']->getItemById($id);
            $response = $this->successJsonResponse($item);
        } catch(FruitBasketException $e) {
            $response = $this->errorJsonResponse($e->getMessage());
        } catch (Exception $e) {
            $this->app['monolog']->critical($e);
            $response = $this->errorJsonResponse($e->getMessage());
        }
        
        return $response;
    }
    
    /**
     * Rename item by id.
     * 
     * @param Request $request
     * @param integer $id
     * @return JsonResponse
     */
    public function renameItemAction(Request $request, $id) {
        if($requestData = $this->getParameterBag($request)) {
            $name = $requestData->get('name');

            try {
                $this->app['item_service']->renameItemById($name, $id);
                $response = $this->successJsonResponse();
            } catch(FruitBasketException $e) {
                $response = $this->errorJsonResponse($e->getMessage());
            } catch (Exception $e) {
                $this->app['monolog']->critical($e);
                $response = $this->errorJsonResponse($e->getMessage());
            }
        } else {
            $response = $this->errorJsonResponse();
        }
        
        return $response;
    }
    
    /**
     * Remove item by id.
     * 
     * @param type $id
     * @return JsonResponse
     */
    public function removeItemAction($id) {
        try {
            $this->app['item_service']->removeItemById($id);
            $response = $this->successJsonResponse();
        } catch(FruitBasketException $e) {
            $response = $this->errorJsonResponse($e->getMessage());
        } catch (Exception $e) {
            $this->app['monolog']->critical($e);
            $response = $this->errorJsonResponse($e->getMessage());
        }
        
        return $response;
    }
}
