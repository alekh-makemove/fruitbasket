<?php
namespace FruitBasket\Controller;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController {

    protected function errorJsonResponse($errorMessage = 'Sorry but something wrong.', $statusCode = JsonResponse::HTTP_BAD_REQUEST) {
        $result = new JsonResponse();
        $result->setData(['errorMessage' => $errorMessage]);
        $result->setStatusCode($statusCode);

        return $result;
    }

    protected function successJsonResponse(array $data = null) {
        $result = new JsonResponse();
        if ($data !== null) {
            $result->setData($data);
        }

        return $result;
    }

    protected function getParameterBag(Request $request) {
        $requestContent = json_decode($request->getContent(), true);
        return ($requestContent !== null) ? new ParameterBag($requestContent) : false;
    }

}
