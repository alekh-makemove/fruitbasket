<?php
namespace FruitBasket\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use FruitBasket\Controller\v1_0\BasketController;

class BasketControllerProvider implements ControllerProviderInterface {

    public function connect(Application $app) {
        $app['basket_controller'] = $app->share(function() use($app) {
            return new BasketController($app);
        });

        $basket = $app['controllers_factory'];
        $basket->post('/add', 'basket_controller:addBasketAction')->bind('add_basket');
        $basket->get('/list', 'basket_controller:basketsListAction')->bind('baskets_list');
        $basket->get('/{id}', 'basket_controller:getBasketAction')
                ->assert('id', '\d+')
                ->bind('get_basket');
        $basket->post('/{id}/rename', 'basket_controller:renameBasketAction')
                ->assert('id', '\d+')
                ->bind('rename_basket');
        $basket->post('/{id}/remove', 'basket_controller:removeBasketAction')
                ->assert('id', '\d+')
                ->bind('remove_basket');
        $basket->post('/{basketId}/item/add', 'basket_controller:addItemsToBasketAction')
                ->assert('basketId', '\d+')
                ->bind('add_item_to_basket');
        $basket->post('/{basketId}/item/{itemId}/remove', 'basket_controller:removeItemFromBasketAction')
                ->assert('basketId', '\d+')
                ->assert('itemId', '\d+')
                ->bind('remove_item_from_basket');

        return $basket;
    }

}
