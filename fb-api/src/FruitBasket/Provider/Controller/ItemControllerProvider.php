<?php
namespace FruitBasket\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use FruitBasket\Controller\v1_0\ItemController;

class ItemControllerProvider implements ControllerProviderInterface {

    public function connect(Application $app) {
        $app['item_controller'] = $app->share(function() use($app) {
            return new ItemController($app);
        });

        $item = $app['controllers_factory'];
        $item->post('/add', 'item_controller:addItemAction')->bind('add_item');
        $item->get('/list', 'item_controller:itemsListAction')->bind('items_list');
        $item->get('/{id}', 'item_controller:getItemAction')
                ->assert('id', '\d+')
                ->bind('get_item');
        $item->post('/{id}/rename', 'item_controller:renameItemAction')
                ->assert('id', '\d+')
                ->bind('rename_item');
        $item->post('/{id}/remove', 'item_controller:removeItemAction')
                ->assert('id', '\d+')
                ->bind('remove_item');

        return $item;
    }

}
