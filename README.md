# README #

### What is this repository for? ###

FruitBasket Repository v.1.0

Implementig API for FruitBusket with some specific actions.

### How do I get set up? ###

1. Clone this repository.
2. Create params.php file in /{project_path}/fb-api/app/config/ folder.
   File should based on /{project_path}/fb-api/app/config/params.php.dist.
3. Edit params.php and add your DB parameters.
4. Run composer install in /{project_path}/fb-api folder, to install all needed dependencies.
5. In console run console command:   
   $ php app/console doctrine:database:create
6. In console run console command:   
   $ php app/console doctrine:schema:load

Now you can use API.

API documentation:
https://docs.google.com/spreadsheets/d/1yYJ-u_ZVwTJRXT9gP7jCSDFOpD2Lv7dDXFPNWWP3DtE/edit?usp=sharing